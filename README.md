## Permission Command

sudo chmod -R 777 storage/
sudo chmod -R 777 bootstrap/cache/

## Artisan Commands

php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan module:optimize
php artisan optimize
composer dump-autoload

php artisan migrate
php artisan db:seed
php artisan migrate:refresh --seed (Re-run all the migrations and seeds(*dev only))

## Tests
To run full test suit
    "./vendor/bin/phpunit"