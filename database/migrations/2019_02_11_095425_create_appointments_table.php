<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('a_id');
            $table->date('a_date',2019/02/11);
            $table->time('a_time');
            $table->unsignedInteger('p_id');
            $table->foreign('p_id')->references('p_id')->on('patients');
            $table->unsignedInteger('d_id');
            $table->foreign('d_id')->references('d_id')->on('doctors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
