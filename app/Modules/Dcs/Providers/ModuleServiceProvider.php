<?php

namespace App\Modules\Dcs\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('dcs', 'Resources/Lang', 'app'), 'dcs');
        $this->loadViewsFrom(module_path('dcs', 'Resources/Views', 'app'), 'dcs');
        $this->loadMigrationsFrom(module_path('dcs', 'Database/Migrations', 'app'), 'dcs');
        $this->loadConfigsFrom(module_path('dcs', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('dcs', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
